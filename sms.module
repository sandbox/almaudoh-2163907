<?php

/**
 * @file
 * The core of the SMS Framework. Provides gateway managment and API for
 * sending and receiving SMS messages.
 */

// Direction codes
define('SMS_DIR_NONE',  0);
define('SMS_DIR_OUT',   1);
define('SMS_DIR_IN',    2);
define('SMS_DIR_ALL',   4);

// Message status codes
// 0=Unknown, 2xx=Positive, 3xx=Positive/Neutral (context-dependent), 4xx=Negative
define('SMS_MSG_STATUS_UNKNOWN',      0);
define('SMS_MSG_STATUS_OK',         200);
define('SMS_MSG_STATUS_DELIVERED',  202);
define('SMS_MSG_STATUS_QUEUED',     302);
define('SMS_MSG_STATUS_ERROR',      400);
define('SMS_MSG_STATUS_NOCREDIT',   402);
define('SMS_MSG_STATUS_EXPIRED',    408);

// Gateway response codes
// 0=Unknown, 2xx=Positive, 4xx=Negative(likely client err), 5xx=Negative(likely gateway err)
define('SMS_GW_UNKNOWN_STATUS',      0);
define('SMS_GW_OK',                200);
define('SMS_GW_ERR_AUTH',          401);
define('SMS_GW_ERR_INVALID_CALL',  400);
define('SMS_GW_ERR_NOT_FOUND',     404);
define('SMS_GW_ERR_MSG_LIMITS',    413);
define('SMS_GW_ERR_MSG_ROUTING',   502);
define('SMS_GW_ERR_MSG_QUEUING',   408);
define('SMS_GW_ERR_MSG_OTHER',     409);
define('SMS_GW_ERR_SRC_NUMBER',    415);
define('SMS_GW_ERR_DEST_NUMBER',   416);
define('SMS_GW_ERR_CREDIT',        402);
define('SMS_GW_ERR_OTHER',         500);

//
define('SMS_CARRIER_DEFAULT', 0);
define('SMS_CARRIER_OVERRIDDEN', 1);
define('SMS_CARRIER_NORMAL', 3);

/**
 * Implements hook_menu().
 */
function sms_menu() {
  $items = array();

  $items['admin/config/smsframework'] = array(
    'title' => 'SMS Framework',
    'description' => 'Control how your site uses SMS.',
    'route_name' => 'sms.admin_base'
  );

  $items['admin/config/smsframework/gateways'] = array(
    'title' => 'Gateway configuration',
    'description' => 'Configure gateways and chose the default gateway.',
    'route_name' => 'sms.gateway_admin',
  );

  $items['admin/config/smsframework/carriers'] = array(
    'title' => 'Carrier configuration',
    'description' => 'Configure supported carriers.',
    'route_name' => 'sms.carrier_admin',
  );

  $items['admin/config/smsframework/bootstrap'] = array(
    'title' => 'Incoming SMS bootstrap',
    'description' => 'Review settings for incoming SMS bootstrap by-pass',
    'route_name' => 'sms.bootstrap_admin',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function sms_permission() {
  return array(
    'administer smsframework' => array(
      'title' => t('administer smsframework'),
      'description' => t('Administer SMS Framework'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function sms_theme() {
  $items['sms_admin_default_form'] =
  $items['sms_carriers_admin_form'] = array(
    'render element' => 'form',
  );

  return $items;
}

/**
 * Implements hook_cron_queue_info().
 */
function sms_cron_queue_info() {
  return array(
    'sms_incoming' => array(
      'worker callback' => 'sms_incoming_queue_worker',
    ),
  );
}

/**
 * Sends a message using the active gateway.
 *
 * @param $number
 *   The destination number.
 *
 * @param $message
 *   The text of the messsage to send.
 *
 * @param $options
 *   An array of dditional properties as defined by gateway modules.
 */
function sms_send($number, $message, $options = array()) {
  $gateway = sms_default_gateway();

  foreach (module_implements('sms_send') as $module) {
    $function = $module . '_sms_send';
    $function($number, $message, $options, $gateway);
  }

  $response = NULL;
  if (function_exists($gateway['send'])) {
    $response = $gateway['send']($number, $message, $options);
  }
  $result = sms_handle_result($response, $number, $message);

  // Post process hook
  foreach (module_implements('sms_send_process') as $module) {
    $function = $module . '_sms_send_process';
    $function('post process', $number, $message, $options, $gateway, $result);
  }

  return $result;
}

/**
 * Handle the response back from the sms gateway.
 */
function sms_handle_result($result, $number, $message) {
  if ($result['status']) {
    return TRUE;
  }
  else {
    $error_message = 'Sending SMS to %number failed.';
    $variables['%number'] = $number;
    if ($result['message']) {
      $error_message .= ' The gateway said ' . $result['message'];
      if (!empty($result['variables'])) {
        $variables = array_merge($variables, $result['variables']);
      }
    }
    watchdog('sms', $error_message, $variables, WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Queue worker callback for queued incomming messages.
 */
function sms_incoming_queue_worker($item) {
  sms_incoming($item['number'], $item['message'], $item['options']);
}

/**
 * Callback for incoming messages. Allows gateways modules to pass messages in
 * a standard format for processing.
 *
 * @param $number
 *   The sender's mobile number.
 *
 * @param $message
 *   The content of the text message.
 */
function sms_incoming($number, $message, $options = array()) {
  if (module_exists('rules')) {
    $options += array('number' => $number, 'message' => $message);
    rules_invoke_event('sms_incoming', $options);
  }

  // Execute three phases
  module_invoke_all('sms_incoming', 'pre process', $number, $message, $options);
  module_invoke_all('sms_incoming', 'process', $number, $message, $options);
  module_invoke_all('sms_incoming', 'post process', $number, $message, $options);
}

/**
 * Callback for incoming message receipts. Allows gateways modules to pass
 * message receipts in a standard format for processing, and provides a basic
 * set of status codes for common code handling.
 *
 * Allowed message status codes are defined as constants at the top of this module.
 *
 * The gateway code and string will often be provided in the $options array as
 * 'gateway_message_status' and 'gateway_message_status_text'.
 *
 * @param string $number
 * The sender's mobile number.
 *
 * @param string $reference
 * Unique message reference code, as provided when message is sent.
 *
 * @param string $message_status
 * An SMS Framework message status code, as per the defined constants.
 *
 * @param array $options
 * Extended options passed by the receipt receiver.
 */
function sms_receipt($number, $reference, $message_status = SMS_GW_UNKNOWN_STATUS, $options = array()) {
  // Execute three phases
  module_invoke_all('sms_receipt', 'pre process', $number, $reference, $message_status, $options);
  module_invoke_all('sms_receipt', 'process', $number, $reference, $message_status, $options);
  module_invoke_all('sms_receipt', 'post process', $number, $reference, $message_status, $options);
}

/**
 * Returns the current default gateway.
 */
function sms_default_gateway() {
  return sms_gateways('gateway', sms_default_gateway_id());
}

function sms_default_gateway_id() {
  $config = \Drupal::config('sms.settings');
  return $config->get('default_gateway');
}

/**
 * Implements hook_gateway_info().
 */
function sms_gateway_info() {
  return array(
    'log' => array(
      'name' => t('Log only'),
      'send' => 'sms_send_log',
    ),
  );
}

/**
 * Log sms message.
 *
 * @param $number
 *   Mobile numbers message sent to.
 * @param $message
 *   Message sent.
 * @param $options
 *   Associative array of options passed to gateway.
 */
function sms_send_log($number, $message, $options) {
  watchdog('sms', 'SMS message sent to %number with the text: @message', array('%number' => $number, '@message' => $message), WATCHDOG_INFO);
  return array('status' => TRUE);
}


/**
 * SMS gateway menutitle callback.
 */
function sms_admin_gateway_title($gateway_id) {
  $gateway = sms_gateways('gateway', $gateway_id);
  return sprintf('%s gateway', $gateway['name']);
}

/**
 * Get a list of all gateways
 *
 * @param $op
 *   The format in which to return the list. When set to 'gateway' or 'name',
 *   only the specified gateway is returned. When set to 'gateways' or 'names',
 *   all gateways are returned.
 *
 * @param $gateway
 *   A gateway identifier string that indicates the gateway to return. Leave at default
 *   value (NULL) to return all gateways.
 *
 * @return
 *   Either an array of all gateways or a single gateway, in a variable format.
 */
function sms_gateways($op = 'gateways', $gateway = NULL) {
  list($_gateways, $_names) = _gateways_build();

  switch ($op) {
    case 'gateways':
      return $_gateways;
    case 'gateway':
      $return = empty($_gateways[$gateway]) ? array() : $_gateways[$gateway];
      $return['identifier'] = $gateway;
      return $return;
    case 'names':
      return $_names;
    case 'name':
      return $_names[$gateway];
  }
}

function _gateways_build() {
  // @todo Implement caching here
  $_gateways = array();
  $_names = array();

  $gateway_array = module_invoke_all('gateway_info');
  $config = \Drupal::config('sms.settings');
  $settings = $config->get('gateway_settings');
  foreach ($gateway_array as $identifier => $info) {
    $info['configuration'] = $settings ? $settings->get($identifier) : array();
    $_gateways[$identifier] = $info;
    $_names[$identifier] = $info['name'];
  }

  asort($_names);

  return array($_gateways, $_names);
}

/**
 * Form builder for send sms form.
 *
 * Generates a SMS sending form and adds gateway defined elements. The form
 * array that is returned can be merged with an existing form using
 * array_merge().
 *
 * @param $required
 *   Specify if the user is required to provide information for the fields.
 *
 * @see sms_send_form_submit_validate()
 * @see sms_send_form_submit_submit()
 */
function sms_send_form($required = FALSE) {
  $gateway = sms_default_gateway();
  $form['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#size' => 40,
    '#maxlength' => 16,
    '#required' => $required,
  );

  // Add gateway defined fields
  if (!empty($gateway['send form']) && function_exists($gateway['send form'])) {
    $form['gateway']['#tree'] = TRUE;
    $form['gateway'] = array_merge($gateway['send form']($required), $form['gateway']);
  }

  return $form;
}

/**
 * Form validation handler for sms_send_form().
 *
 * @see sms_send_form()
 * @see sms_send_form_submit()
 */
function sms_send_form_validate($form, &$form_state) {
  $not_valid = sms_validate_number(sms_formatter($form_state['values']['number']), $form_state['values']['gateway']);
  if ($not_valid) {
    form_set_error('number', t($not_valid));
  }
}

/**
 * Form submission handler for sms_send_form().
 *
 * @see sms_send_form()
 * @see sms_send_form_validate()
 */
function sms_send_form_submit($form, &$form_state) {
  $form_state['values']['number'] = sms_formatter($form_state['values']['number']);
  sms_send($form_state['values']['number'], $form_state['values']['message'], $form_state['values']['gateway']);
}

/******************************************************************************
 * SMS Carrier Functions
 *
 * @todo - consider moving this to email gateway, unless there is a reason to
 * have these functions without the email gateway?
 *****************************************************************************/

/**
 * Get a list of all carriers
 * @todo - store carriers in config instead of database
 */
function sms_carriers($domain = NULL) {
  $default_carriers = module_invoke_all('sms_carriers');
  $carriers = array();

  // Load default carriers from code
  foreach ($default_carriers as $id => $carrier) {
    $carriers[$id] = array('name' => $carrier, 'type' => SMS_CARRIER_DEFAULT);
  }

  // Load overridden carriers from database
  $result = db_query("SELECT name, domain FROM {sms_carriers}");

  foreach ($result as $carrier) {
    if (in_array($carrier->domain, array_keys($carriers))) {
      $type = SMS_CARRIER_OVERRIDDEN;
    }
    else {
      $type = SMS_CARRIER_NORMAL;
    }

    $carriers[$carrier->domain] = array(
      'name' => $carrier->name,
      'type' => $type,
    );
  }

  foreach (Drupal::config('sms.settings')->get('enabled_carriers') as $carrier) {
    if (is_array($carriers[$carrier])) {
      $carriers[$carrier]['status'] = 1;
    }
  }

  if ($domain) {
    $carriers[$domain]['domain'] = $domain;
    return $carriers[$domain];
  }

  return $carriers;
}

/**
 * Load a single carrier
 */
function carrier_load($domain) {
  return sms_carriers($domain);
}

/**
 * Save a carrier.
 */
function carrier_save($domain, $edit) {
  if (!empty($domain)) {
    $carrier = carrier_load($domain);

    if ($carrier['type'] == SMS_CARRIER_DEFAULT) {
      $edit['status'] = 1;
      drupal_write_record('sms_carriers', $edit);
    }
    elseif (!empty($edit['domain'])) {
      //Case for when the domain name hasn't changed
      if ($edit['domain'] == $domain) {
        drupal_write_record('sms_carriers', $edit, 'domain');
      }
      //Case for when the domain has changed
      else {
        carrier_delete($domain);
        drupal_write_record('sms_carriers', $edit);
      }

      // TODO: we need more logic to figure out when someone is changing the domain name
    }
  }
  else {
    $edit['status'] = 1;
    drupal_write_record('sms_carriers', $edit);
  }
}

function carrier_delete($domain) {
  db_delete('sms_carriers')
    ->condition('domain', $domain)
    ->execute();

  //removes carrier from config also
  $config = \Drupal::config('sms.settings');
  $enabled_carriers = $config->get('enabled_carriers');
  foreach ($enabled_carriers as $i => $carrier) {
    if ($carrier == $domain) {
      unset($enabled_carriers[$i]);
      break;
    }
  }
  $config->set('enabled_carriers', $enabled_carriers)->save();
}

/******************************************************************************
 * HELPER FUNCTIONS
 */

/**
 * Formats a number for display.
 */
function sms_format_number(&$number, $options = array()) {
  $gateway = sms_default_gateway();

  if (!empty($gateway['format number']) && function_exists($gateway['format number'])) {
    return $gateway['format number']($number, $options);
  }
  else {
    return $number;
  }
}

/**
 * Converts various sms formats into a common format for use in this module.
 *
 * @param $number
 *   The sms number
 * @param $format
 *   Undefined - @todo: decide if this is needed.
 */
function sms_formatter($number, $format = 1) {
  // Remove non-number characters
  $number = preg_replace("/[^0-9]/", '', $number);

  /*
   @todo - the only length specification in the international numbering plan is that
   numbers should be a maximum of 15 digits.

   http://en.wikipedia.org/wiki/E.164

   if (strlen($number) > 16) {
   if ($number[0] == 1) {
   $number = ltrim($number, 1);
   }
   else {
   return FALSE;
   }
   }
   elseif (strlen($number) < 10) {
   return FALSE;
   }
   */

  return $number;
}

/**
 * Validates a phone number. Passes number to active gateway for further
 * validation if neccessary.
 */
function sms_validate_number(&$number, $options = array()) {
  if (!strlen($number)) {
    return t('The phone number is invalid.');
  }

  // Allow the active gateway to provide number validation
  $gateway = sms_default_gateway();
  // TODO we neeed some sort of validation this isn't working properly.
  if (isset($gateway['validate_number'])) {
    if (function_exists($gateway['validate number']) && $error = $gateway['validate number']($number, $options)) {
      return $error;
    }
  }
  if (module_exists('sms_valid')) {
    if ($error = sms_valid_validate($number, $options)) {
      return $error;
    }
  }
  return NULL;
}

/**
 * Render a direction code
 *
 * @param $out bool Outgoing allowed or not
 * @param $in  bool Incoming allowed or not
 * @return const The constant that defines this direction combination. Usually an integer value.
 */
function sms_dir($out, $in) {
  if ( $out &&   $in) {
    return SMS_DIR_ALL;
  }
  if ( $out && !$in) {
    return SMS_DIR_OUT;
  }
  if (!$out &&   $in) {
    return SMS_DIR_IN;
  }
  if (!$out && !$in) {
    return SMS_DIR_NONE;
  }
}

/**
 * Returns an array of E.164 international country calling codes
 *
 * @return array Associative array of country calling codes and country names.
 */
function sms_country_codes() {
  return \Drupal::config('sms.settings')->get('country_codes');
}

/**
 * Returns HTML for the admin default gateway form.
 *
 * @param $form
 *  An form array.
 *
 * @ingroup themeable
 */
function theme_sms_admin_default_form($variables) {
  $form = $variables['form'];
  $rows = array();
  foreach ($form as $name => $element) {
    if (is_array($element) and isset($element['id']) and is_array($element['id'])) {
      $rows[] = array(
        drupal_render($form['default'][$element['id']['#markup']]),
        check_plain($name),
        $element['configure']['#markup'],
      );
      unset($form[$name]);
    }
  }
  $header = array(t('Default'), t('Name'), array(
      'data' => t('Operations'),
      'colspan' => 1,
    ));
  $output = '';
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Returns HTML for the sms carriers admin form.
 *
 * @param $form
 *  A form array.
 *
 * @ingroup themeable
 */
function theme_sms_carriers_admin_form($variables) {
  $form = $variables['form'];

  $header = array(t('Enabled'), t('Carrier'), t('Domain'), t('Actions'));

  $rows = array();
  foreach (element_children($form['status']) as $element) {
    $name = "<div class='carrier'>";
    $name .= "<strong>{$form['status'][$element]['#title']}</strong>";
    $name .= "<div class='description'>{$form['status'][$element]['#description']}</div>";
    $name .= "</div>";
    unset($form['status'][$element]['#title']);
    unset($form['status'][$element]['#description']);
    $row = array(
        'status' => drupal_render($form['status'][$element]),
        'name' => $name,
        'domain' => drupal_render($form['domain'][$element]),
        'actions' => drupal_render($form['actions'][$element]),
    );
    $rows[] = $row;
  }
  $output = '';
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array( 'class' => array('sms'), 'id' => 'sms-form-table' )));
  $output .= drupal_render($form['submit']);
  $output .= drupal_render_children($form);

  return $output;
}


