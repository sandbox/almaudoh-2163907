<?php
/**
 * Created by PhpStorm.
 * User: almaudoh
 * Date: 12/27/13
 * Time: 4:33 PM
 */

namespace Drupal\sms_blast;


use Drupal\Core\Form\FormBase;

class SmsBlastFormController extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'sms_blast_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state)
  {
    $form['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#cols' => 60,
      '#rows' => 5,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state)
  {
    $result = db_select('sms_user', 'su')
                ->fields('su', array('uid'))
                ->condition('status', 2)
                ->execute();

    if ($result->rowCount() != 0) {
      foreach ($result as $row) {
        sms_user_send($row->uid, $form_state['values']['message']);
      }
      drupal_set_message(t('The message was sent to %count users.', array('%count' => $result->rowCount())));
    }
    else {
      drupal_set_message(t('There are 0 users with confirmed phone numbers. The message was not sent.'));
    }
  }
}